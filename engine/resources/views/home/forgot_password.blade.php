@extends("layouts.login")

@section("content")
<div class="row">
    <div class="col-md-3 center">
        <div class="login-box">
            <a href="index.html" class="logo-name text-lg text-center">Adventure Earth</a>
            <p class="text-center m-t-md">Enter your e-mail address below to reset your password</p>
            {!! Form::open(["url" => "/home/forgot-password","method" => "POST"]) !!}
                <div class="form-group">
                    {!! Form::text('email', Input::old('email'), array('class' => 'form-control text-input','placeholder' => 'Email')) !!}
                    @if ($errors->has('email'))
                    <div class="alert alert-danger" style="">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-success btn-block">Submit</button>
                <a href="{{ url('/auth/login') }}" class="btn btn-default btn-block m-t-md">Back</a>
            {!! Form::close() !!}
            <p class="text-center m-t-xs text-sm">2016 &copy; Adventure Earth.</p>
        </div>
    </div>
</div><!-- Row -->
@endsection