@extends("layouts.default")

@section("heading", "Manage Sticker Collections")

@section("breadcrumb")
<ol class="breadcrumb container">
    <li><a href="{{ url('/sticker-collections') }}">Sticker Collection</a></li>
    <li class="active">Manage</li>
</ol>
@endsection

@section("content")
<div class="row">
    <div class="col-md-2">
        <a href="{{ url('sticker-collections/create/0') }}" class="btn btn-success btn-block">Create Sticker Collection</a><br />
        <ul class="list-unstyled mailbox-nav">
            <li class="active"><a href="{{ url('sticker-collections') }}"><i class="icon-wrench"></i>Manage</a></li>
            {{--<li><a href="{{ url('sticker-collections/daft') }}"><i class="icon-drawer"></i>Draft <span class="badge badge-info pull-right"></span></a></li>--}}
            <li><a href="{{ url('sticker-collections/trash') }}"><i class="icon-trash"></i>Trash <span class="badge badge-danger pull-right">{{ $trashCount }}</span></a></li>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="row">
            @if($models->count() > 0)
                @foreach($models as $model)
                <div class="col-md-4">
                    <div class="panel panel-white">
                        <div class="panel-heading panel-heading-with-stickers">
                            <h3 class="panel-title" data-toggle="tooltip" data-original-title="{{ (strlen($model->name) > 0)? $model->name : 'Untitled' }}">{{ (strlen($model->name) > 0)? $model->name : 'Untitled' }}</h3>
                            <div class="panel-control">
                            <a href="{{ url('sticker-collections/update',$model->id) }}" data-toggle="tooltip" title="" data-original-title="Edit"><i class="icon-note text-success"></i></a>
                                @if($model->locked)
                                &nbsp;&nbsp;<i class=" icon-lock text-default"></i>
                                @else
                                <a href="{{ url('sticker-collections/modal-delete',$model->id) }}" data-toggle="modal" data-target="#delete_confirm"><span data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="icon-trash text-danger"></i></span></a>
                                @endif
                            </div>
                        </div>
                        <div class="panel-body text-center"><img src="{{ $model->getAvatarStickerImageSrc() }}" class="img-responsive" /></div>
                        <div class="panel-footer">
                            <ul class="list-unstyled">
                                <li><i class="icon-flag"></i> Status<div class="pull-right">{!! $model->getStatusLabel() !!}</div></li>
                                <li><i class="icon-wallet"></i> Price<div class="text-info pull-right">${{ number_format($model->price,2) }}</div></li>
                                <li><i class="icon-graph"></i> Sales Volume<div class="pull-right">{{ number_format($model->sale_volume,0) }}</div></li>
                                <li><i class="icon-cloud-download"></i> Download<div class="pull-right">{{ number_format($model->download_count,0) }}</div></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                <div class="col-md-4 center">
                    <div class="details text-center">
                        <span class="fa-stack fa-5x fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-image fa-stack-1x" style="color: white;"></i>
                        </span>
                        <h3 class="text-info">Create Sticker Collection</h3>
                        <p>Please click the button below to create first sticker collection.</p>
                        <a href="{{ url('sticker-collections/create') }}" class="btn btn-success">Create Sticker Collection</a><br />
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="banner_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@endsection

@section('footer_scripts')
<script>
$(document).ready(function() {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@endsection