@extends('layouts.default')

@section('heading', 'Create Sticker Collection')

@section('breadcrumb')
    <ol class="breadcrumb container">
         <li><a href="{{ url('/sticker-collections') }}">Sticker Collection</a></li>
         <li><a href="{{ url('/sticker-collections') }}">Manage</a></li>
         <li class="active">Create Sticker Collection</li>
     </ol>
@endsection

@section('content')
    @include('sticker-collections._form', array('model'=>$model))
@endsection