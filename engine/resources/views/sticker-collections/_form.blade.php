<div class="panel panel-white">
    <div class="panel-heading"><h3 class="panel-title">Sticker Collection Information</h3></div>
    <div class="panel-body">
        <div class="col-md-12">
            <form method="POST" action="{{ Request::url() }}" enctype="multipart/form-data" class="form-horizontal">
                <!-- CSRF Token -->
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="form-group {{ $errors->first('name', 'has-error') }}">
                    <label for="name" class="col-sm-2 control-label">Name *</label>
                    <div class="col-sm-10">
                        <input id="name" name="name" type="text" placeholder="Title" class="form-control required" value="{!! $model->name !!}"/>
                        <small class="help-block">{{ $errors->first('name', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group {{ $errors->first('description', 'has-error') }}">
                    <label for="link" class="col-sm-2 control-label">Description *</label>
                    <div class="col-sm-10">
                        <textarea id="description" name="description" class="form-control required" placeholder="Description" rows="4=6">{!! $model->description !!}</textarea>
                        <small class="help-block">{{ $errors->first('description', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group {{ $errors->first('price', 'has-error') }}">
                    <label for="price" class="col-sm-2 control-label">Price *</label>
                    <div class="col-sm-10">
                        <select id="price" class="form-control" title="Select Price..." name="price">
                            @foreach($model->getPriceList() as $value => $label)
                                <option value="{{ $value }}" {{ $model->price==$value? 'selected="selected"' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                        <small class="help-block">{{ $errors->first('price', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group {{ $errors->first('ios_inapp_id', 'has-error') }}">
                    <label for="ios_inapp_id" class="col-sm-2 control-label">AppStore item identifier *</label>
                    <div class="col-sm-10">
                        <input id="ios_inapp_id" name="ios_inapp_id" type="text" placeholder="identifier" class="form-control required" value="{!! $model->ios_inapp_id !!}" {{ ((bool)$model->locked)? 'disabled="disabled"' : '' }}/>
                        <small class="help-block">{{ $errors->first('ios_inapp_id', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group {{ $errors->first('ios_inapp_id', 'has-error') }}">
                    <label for="android_inapp_id" class="col-sm-2 control-label">GooglePlay item identifier *</label>
                    <div class="col-sm-10">
                        <input id="android_inapp_id" name="android_inapp_id" type="text" placeholder="identifier" class="form-control required" value="{!! $model->android_inapp_id !!}" {{ ((bool)$model->locked)? 'disabled="disabled"' : '' }}/>
                        <small class="help-block">{{ $errors->first('android_inapp_id', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group {{ $errors->first('status', 'has-error') }}">
                    <label for="status" class="col-sm-2 control-label">Status *</label>
                    <div class="col-sm-10">
                        <select id="status" class="form-control" title="Select Status..." name="status" {{ ((bool)$model->locked)? 'disabled="disabled"' : '' }}>
                            <option value="">Select</option>
                            <option value="1" {{ $model->status==1? 'selected="selected"' : '' }}>Published</option>
                            <option value="0" {{ $model->status==0? 'selected="selected"' : '' }}>Unpublished</option>
                        </select>
                        <small class="help-block">{{ $errors->first('status', ':message') }}</small>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">{{ ($model->exist)? 'Update' : 'Save' }}</button>
                        <a href="{{ url('sticker-collections') }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

        @if(!(bool)$model->locked)
        <div class="col-md-12">
            <div id="uploader">
                <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
            </div>
        </div>
        @endif
    </div>
</div>

<h2>Stickers</h2>
<hr />
<div class="row" id="sticker_control_panel" style="display: none">
    <div class="col-md-4">
        <div class="panel panel-white">
            <div class="panel-heading"><h3 class="panel-title">Collection Avatar</h3></div>
            <div class="panel-body">
                <img id="collection_avatar_image" src="{{ $model->getAvatarStickerImageSrc() }}" class="img-responsive" style="width: 100%;border: 1px solid #eee" />
                {{--@if(!(bool)$model->locked)--}}
                <br />
                <div class="text-right" id="avatar-change-panel">
                    <a href="javascript:changeAvatarSelector();" class="btn btn-success">Change Avatar</a>
                </div>
                <div class="text-right" id="avatar-action-panel">
                    <a href="javascript:cancelAvatarSelector();" class="btn btn-default">Cancel</a>
                    <a href="javascript:updateAvatarSelector();" class="btn btn-success">Done</a>
                </div>
                {{--@endif--}}
            </div>
        </div>

        <div class="panel panel-white">
            <div class="panel-heading"><h3 class="panel-title">Collection Thumbnails</h3></div>
            <div class="panel-body">
                <div class="row" id="stickers_thumbnail_place">
                    @if($model->stickersThumbnail->count() > 0)
                        @foreach($model->stickersThumbnail as $sticker)
                        <div class="col-md-4">
                            <img src="{{ $sticker->getImageThumbnailSrc() }}" class="img-responsive" />
                        </div>
                        @endforeach

                        <?php
                        $currentAttributeCount = $model->stickersThumbnail->count();

                        if($currentAttributeCount < 2) {
                            for($i=$currentAttributeCount;$i<2;$i++) {
                                ?>
                                <div class="col-md-4">
                                    <img src="{{ url('/assets/images/sticker-holder.png') }}" class="img-responsive" />
                                </div>
                                <?php
                            }
                        }
                        ?>
                    @else
                        <div class="col-md-4">
                            <img src="{{ url('/assets/images/sticker-holder.png') }}" class="img-responsive" />
                        </div>
                        <div class="col-md-4">
                            <img src="{{ url('/assets/images/sticker-holder.png') }}" class="img-responsive" />
                        </div>
                    @endif
                </div>

                {{--@if(!(bool)$model->locked)--}}
                <br />
                <div class="text-right" id="thumbnail-change-panel">
                    <a href="javascript:changeThumbnailSelector();" class="btn btn-success">Change Thumbnail</a>
                </div>
                <div class="text-right" id="thumbnail-action-panel">
                    <a href="javascript:cancelThumbnailSelector();" class="btn btn-default">Cancel</a>
                    <a href="javascript:updateThumbnailSelector();" class="btn btn-success">Done</a>
                </div>
                {{--@endif--}}
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row" id="sticker_sortable">
            @if($model->stickers->count() > 0)
                @foreach($model->stickers as $sticker)
                <div class="col-md-3 col-sm-3 col-xs-4 sticker-item" data-id="{{ $sticker->id }}">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <img src="{{ $sticker->getImageThumbnailSrc() }}" class="img-responsive" />

                            @if(!(bool)$model->locked)
                            <div class="panel-body-tool">
                                <div class="pull-left text-left">
                                    <a href="javascript:void(0);" class="btn btn-xs btn-info sticker-item-move-handle">
                                        <span data-toggle="tooltip" title="" data-original-title="Move">
                                            <i class="icon-cursor-move"></i>
                                        </span>
                                    </a>
                                </div>
                                <div class="pull-right text-right">
                                    <a href="{{ url('sticker-collections/sticker-modal-delete',$sticker->id) }}" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#delete_confirm">
                                        <span data-toggle="tooltip" title="" data-original-title="Delete">
                                            <i class="icon-trash"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            @endif

                            <div class="panel-body-highlight"></div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="banner_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>

@section('header_css')
@parent
    <link href={{ asset("assets/plugins/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css") }} rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
@parent
    <script src="{{ asset("assets/plugins/plupload/js/plupload.full.min.js") }}"></script>
    <script src="{{ asset("assets/plugins/plupload/js/jquery.plupload.queue/jquery.plupload.queue.min.js") }}"></script>
    <script src="{{ asset("assets/plugins/nestable/jquery.nestable.js") }}"></script>


    <script type="text/javascript">
        $(window).load(function() {
            $('#sticker_control_panel').fadeIn(500);
        });
    </script>

    <!-- Upload Sticker Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            function initUploader() {
                // Setup html5 version
                $("#uploader").pluploadQueue({
                    // General settings
                    runtimes : 'html5,flash,silverlight,html4',
                    url : "{{ url('sticker-collections/upload-sticker',[$model->id]) }}",
                    dragdrop: true,
                    filters : {
                        // Maximum file size
                        max_file_size : '8mb',
                        // Specify what files to browse for
                        mime_types: [
                            {title : "Image files", extensions : "jpg,gif,png"}
                        ]
                    },

                    // Flash settings
                    flash_swf_url : '/assets/plugins/plupload/js/Moxie.swf',

                    // Silverlight settings
                    silverlight_xap_url : '/assets/plugins/plupload/js/Moxie.xap',
                    init : {
                        UploadComplete: function(up, files) {
                            $.ajax({
                                url:'{{ url('sticker-collections/update-sticker-zip-file',[$model->id]) }}'
                            });

                            $.ajax({
                                url: '{{ url('sticker-collections/update',[$model->id]) }}',
                                success: function(res) {
                                    var elm = $(res).find('#sticker_sortable');

                                    $('#sticker_sortable').html(elm.html());
                                }
                            });

                            up.destroy();
                            initUploader();
                        }
                    }
                });
            }

            initUploader();
        });
    </script>

    @if(!(bool)$model->locked)
    <!-- sortable sticker -->
    <script style="text/javascript">
        $('#sticker_sortable').sortable({
            handle: '.sticker-item-move-handle',
            stop: function(event,ui) {
                var sorted = [];

                $('.sticker-item').each(function(index,element) {
                    sorted.push({id:$(element).data('id')})
                });

                var jsonSorted = JSON.stringify(sorted);

                $.ajax({
                    method: 'post',
                    url: "{{ url('sticker-collections/sticker-order',[$model->id]) }}",
                    data: { order: jsonSorted },
                    success: function(data) {

                    }
                });
            }
        });
    </script>

    @endif

    <!-- change avatar -->
    <script class="text/javascript">
        var currentAvatarStickerId = '{{ $model->avatarSticker? $model->avatarSticker->id : null }}';
        var tempAvatarStickerId = null;

        var avatarChangePanel = $('#avatar-change-panel').show();
        var avatarActionPanel = $('#avatar-action-panel').hide();

        function changeAvatarSelector() {
            avatarActionPanel.show();
            avatarChangePanel.hide();
            thumbnailChangePanel.children('.btn').addClass('disabled');

            var stickers = $('#sticker_sortable');

            // Add current selected avatar
            if(currentAvatarStickerId != null) {
                $('.sticker-item').each(function(index,element) {
                    if($(this).data('id') == currentAvatarStickerId) {
                        $(this).addClass('ui-selected');
                    }
                });
            }

            stickers.children('.sticker-item').addClass('sticker-item-selectable');
            stickers.selectable({
                stop: function(event, ui) {
                    // prevent multiple select
                    $(event.target).children('.ui-selected').not(':first').removeClass('ui-selected');

                    tempAvatarStickerId = $( ".ui-selected", this ).first().data('id');
                 }
            });
        }

        function cancelAvatarSelector() {
            avatarActionPanel.hide();
            avatarChangePanel.show();
            thumbnailChangePanel.children('.btn').removeClass('disabled');

            var stickers = $('#sticker_sortable');
            stickers.selectable('destroy');
            stickers.children('.sticker-item')
                .removeClass('sticker-item-selectable')
                .removeClass('ui-selected');

            tempAvatarStickerId = null;
        }

        function updateAvatarSelector() {
            avatarActionPanel.hide();
            avatarChangePanel.show();
            thumbnailChangePanel.children('.btn').removeClass('disabled');

            var stickers = $('#sticker_sortable');
            stickers.selectable('destroy');
            stickers.children('.sticker-item')
                .removeClass('sticker-item-selectable')
                .removeClass('ui-selected');

            updateAvatar();
        }

        function updateAvatar() {
            currentAvatarStickerId = (tempAvatarStickerId == null)? currentAvatarStickerId : tempAvatarStickerId;
            tempAvatarStickerId = null;

            $.ajax({
                method: 'post',
                url: "{{ url('sticker-collections/update-avatar',[$model->id]) }}",
                data: { sticker_id: currentAvatarStickerId },
                success: function(data) {
                    $('#collection_avatar_image').attr('src',data.image);
                }
            });
        }
    </script>

    <!-- change thumbnail -->
    <script type="text/javascript">
        function removeA(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax= arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }

        var stickerLimit = 2;
        var currentThumbnailStickersId = [];

        @if($model->stickersThumbnail->count() > 0)
            @foreach($model->stickersThumbnail as $sticker)
                currentThumbnailStickersId.push('{{ $sticker->id }}');
            @endforeach
        @endif

        var tempThumbnailStickersId = [];

        var thumbnailChangePanel = $('#thumbnail-change-panel').show();
        var thumbnailActionPanel = $('#thumbnail-action-panel').hide();

        function changeThumbnailSelector() {
            thumbnailActionPanel.show();
            thumbnailChangePanel.hide();
            avatarChangePanel.children('.btn').addClass('disabled');

            var stickers = $('#sticker_sortable');

            // Add current selected avatar
            $.each(currentThumbnailStickersId, function(i,val) {
                $('.sticker-item[data-id="'+val+'"]').addClass('ui-selected');
                tempThumbnailStickersId.push(val);
            });

            stickers.children('.sticker-item').addClass('sticker-item-selectable');
            stickers.children('.sticker-item').click(function(e) {
                $(this).toggleClass("ui-selected");

                if(!$(this).hasClass('ui-selected')) {
                    removeA(tempThumbnailStickersId,$(this).data('id'));
                } else {
                    tempThumbnailStickersId.push($(this).data('id'));
                }

                if ($(".ui-selected, .ui-selecting").length > stickerLimit) {
                    $('.sticker-item[data-id="'+tempThumbnailStickersId[0]+'"]').removeClass('ui-selected');
                    tempThumbnailStickersId.splice(0, 1);
                }
            });

//            stickers.selectable({
//                selecting: function(event, ui) {
//                    if ($(".ui-selected, .ui-selecting").length > stickerLimit) {
//                        $(ui.selecting).removeClass("ui-selecting");
//                    }
//                },
//                stop: function(event, ui) {
//                    // prevent multiple select
//                    tempThumbnailStickersId = [];
//
//                    $.each($(".ui-selected",this), function(i, val) {
//                        tempThumbnailStickersId.push($(val).data('id'));
//                    });
//                 }
//            });
        }

        function cancelThumbnailSelector() {
            thumbnailActionPanel.hide();
            thumbnailChangePanel.show();
            avatarChangePanel.children('.btn').removeClass('disabled');

            var stickers = $('#sticker_sortable');
            stickers.selectable('destroy');
            stickers.children('.sticker-item')
                .removeClass('sticker-item-selectable')
                .removeClass('ui-selected');

            tempThumbnailStickersId = [];
        }

        function updateThumbnailSelector() {
            thumbnailActionPanel.hide();
            thumbnailChangePanel.show();
            avatarChangePanel.children('.btn').removeClass('disabled');

            var stickers = $('#sticker_sortable');
//            stickers.selectable('destroy');

            stickers.children('.sticker-item')
                .unbind('click')
                .removeClass('sticker-item-selectable')
                .removeClass('ui-selected');

            updateThumbnail();
        }

        function updateThumbnail() {
            currentThumbnailStickersId = tempThumbnailStickersId;
            tempThumbnailStickersId = [];

            $.ajax({
                method: 'post',
                url: "{{ url('sticker-collections/update-thumbnails',[$model->id]) }}",
                data: { stickers_id: currentThumbnailStickersId.join(',') },
                success: function(data) {
                    var place = $('#stickers_thumbnail_place').html('');

                    $.each(data.images, function(i,image) {
                        var img = $('<img />').addClass('img-responsive').attr('src',image);
                        var column = $('<div></div>').addClass('col-md-4');
                        column.append(img);
                        place.append(column);
                    });
                }
            });
        }
    </script>
@endsection