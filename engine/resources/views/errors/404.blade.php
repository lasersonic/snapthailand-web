@extends('layouts.error')
<?php

$url = !isset($url)? url('/') : $url;
$title = !isset($title)? 'Dashboard' : $title;
$message = !isset($message)? "We can't find the page you're looking for." : $message;

?>
@section('content')
    <div class="row">
        <div class="col-md-4 center">
            <h1 class="text-xxl text-primary text-center">404</h1>
            <div class="details">
                <h3>Oops ! Something went wrong</h3>
                <p>{{ $message }} <br />Return <a href="{{ $url }}">{{ $title }}</a></p>
            </div>
        </div>
    </div><!-- Row -->
@endsection