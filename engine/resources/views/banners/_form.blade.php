<form id="commentForm" method="POST" action="{{ Request::url() }}" enctype="multipart/form-data" class="form-horizontal">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <fieldset>
        <legend>Banner Image</legend>

        <div class="form-group {{ $errors->first('image', 'has-error') }}">
            <label for="pic" class="col-sm-2 control-label">Banner Image</label>
            <div class="col-sm-10">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        @if(!$model->image)
                        <img src="http://dummyimage.com/640x100/e3e3e3/999999" alt="banner pic">
                        @else
                        <img src="{{ url($model->image) }}" alt="banner pic">
                        @endif
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 640px; max-height: 100px;"></div>
                    <div>
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input id="image" name="image" type="file" class="form-control"/>
                        </span>
                        <a href="#" class="btn btn-danger fileinput-exists"
                           data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
                <small class="help-block">{{ $errors->first('image', ':message') }}</small>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Banner Information</legend>

        <div class="form-group {{ $errors->first('name', 'has-error') }}">
            <label for="name" class="col-sm-2 control-label">Name *</label>
            <div class="col-sm-10">
                <input id="name" name="name" type="text" placeholder="Title" class="form-control required" value="{!! $model->name !!}"/>
                <small class="help-block">{{ $errors->first('name', ':message') }}</small>
            </div>
        </div>

        <div class="form-group {{ $errors->first('link', 'has-error') }}">
            <label for="link" class="col-sm-2 control-label">Link *</label>
            <div class="col-sm-10">
                <input id="link" name="link" type="text" placeholder="http://www.banner.com" class="form-control required" value="{!! $model->link !!}"/>
                <small class="help-block">{{ $errors->first('link', ':message') }}</small>
            </div>
        </div>

        <div class="form-group {{ $errors->first('status', 'has-error') }}">
            <label for="status" class="col-sm-2 control-label">Status *</label>
            <div class="col-sm-10">
                <select id="status" class="form-control" title="Select Status..." name="status">
                    <option value="">Select</option>
                    <option value="1" {{ $model->status==1? 'selected="selected"' : '' }}>Published</option>
                    <option value="0" {{ $model->status==0? 'selected="selected"' : '' }}>Unpublished</option>
                </select>
                <small class="help-block">{{ $errors->first('status', ':message') }}</small>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">{{ ($model->exist)? 'Update' : 'Save' }}</button>
            <a href="{{ url('banners') }}" class="btn btn-default">Cancel</a>
        </div>
    </div>
</form>