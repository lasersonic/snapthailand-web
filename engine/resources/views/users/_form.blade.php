
{!! Form::model($model, ['class'=>'form-horizontal']) !!}
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <fieldset>
        <legend>Account</legend>

        <div class="form-group {{ $errors->first('email', 'has-error') }}">
            <label for="email" class="col-sm-2 control-label">E-mail *</label>
            <div class="col-sm-10">
                {!! Form::text('email', null, ['id','email','placeholder'=>'E-mail','class' => 'form-control required']) !!}
                <small class="help-block">{{ $errors->first('email', ':message') }}</small>
            </div>
        </div>

        <div class="form-group {{ $errors->first('password', 'has-error') }}">
            <label for="password" class="col-sm-2 control-label">Password *</label>
            <div class="col-sm-10">
                {!! Form::password('password', ['id','password','placeholder'=>'Password','class' => 'form-control required']) !!}
                <small class="help-block">{{ $errors->first('password', ':message') }}</small>
            </div>
        </div>

        <div class="form-group {{ $errors->first('confirm_password', 'has-error') }}">
            <label for="confirm_password" class="col-sm-2 control-label">Confirm Password *</label>
            <div class="col-sm-10">
                {!! Form::password('confirm_password', ['id','confirm_password','placeholder'=>'Confirm Password','class' => 'form-control required']) !!}
                <small class="help-block">{{ $errors->first('confirm_password', ':message') }}</small>
            </div>
        </div>

    </fieldset>
    <fieldset>
            <legend>Profile</legend>

        <div class="form-group {{ $errors->first('first_name', 'has-error') }}">
            <label for="first_name" class="col-sm-2 control-label">Name *</label>
            <div class="col-sm-10">
                {!! Form::text('first_name', null, ['id','first_name','placeholder'=>'Name','class' => 'form-control required']) !!}
                <small class="help-block">{{ $errors->first('first_name', ':message') }}</small>
            </div>
        </div>

        <div class="form-group {{ $errors->first('last_name', 'has-error') }}">
            <label for="last_name" class="col-sm-2 control-label">Surname *</label>
            <div class="col-sm-10">
                {!! Form::text('last_name', null, ['id','last_name','placeholder'=>'Surname','class' => 'form-control required']) !!}
                <small class="help-block">{{ $errors->first('last_name', ':message') }}</small>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">{{ ($model->exist)? 'Update' : 'Save' }}</button>
        </div>
    </div>
{!! Form::close() !!}

@section('footer_scripts')
@parent
    <script>
        $("#commentForm").bootstrapValidator({
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email is required'
                        }
                    },
                    required: true,
                    minlength: 3
                },
                first_name: {
                    validators: {
                        notEmpty: {
                            message: 'The name is required'
                        }
                    },
                    required: true,
                    minlength: 3
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'The surname is required'
                        }
                    },
                    required: true,
                    minlength: 3
                }
            }
        });
    </script>
@stop