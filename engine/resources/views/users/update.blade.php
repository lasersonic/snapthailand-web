@extends('layouts.default')

@section('heading', 'Update User')

@section('header_styles')
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
@stop

@section("breadcrumb")
    <ol class="breadcrumb container">
        <li><a href="{{ url('/users') }}">Users</a></li>
        <li class="active">Update User</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        @include('users._form', array('model'=>$model))
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')

@stop