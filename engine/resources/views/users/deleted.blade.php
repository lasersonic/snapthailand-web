@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Deleted Banners List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Breadcrumb --}}
@section('breadcrumb')
<section class="content-header">
    <h1>Deleted Banners</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="dashboard" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Banners</li>
        <li class="active">Deleted Banners</li>
    </ol>
</section>
@stop

{{-- Page content --}}
@section('content')


<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-default">

            <div class="panel-body">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Creator</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($models as $model)
                    	<tr>
                            <td>{!! $model->id !!}</td>
                    		<td>{!! $model->name !!}</td>
            				<td>{!! $model->status !!}</td>
            				<td>{!! $model->created_at->diffForHumans() !!}</td>
            				<td>{!! $model->creator->first_name . ' ' . $model->creator->last_name !!}</td>
            				<td>
            				    <a href="{{ route('restore/banner', $model->id) }}"><i class="livicon" data-name="timer" data-c="#6CC66C" data-hc="#6CC66C" data-size="18"></i></a>
                            </td>
            			</tr>
                    @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

<script>
$(document).ready(function() {
	$('#table').DataTable();
});
</script>
@stop