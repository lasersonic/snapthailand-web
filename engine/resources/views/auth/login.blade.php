@extends("layouts.login")

@section("content")
<div class="row">
    <div class="col-md-3 center">

        <div class="login-box">
            @if(Session::has('flash_error') || Session::has('error'))
                <div class="alert alert-danger" role="alert">{!! Session::get('error') !!}</div>
            @endif

            @if(Session::has('warning'))
                <div class="alert alert-warning" role="alert">{!! Session::get('warning') !!}</div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">{!! Session::get('success') !!}</div>
            @endif

            <a href="/" class="logo-name text-lg text-center">Adventure Earth</a>
            <p class="text-center m-t-md">Please login into your account.</p>
            {!! Form::open(["url" => "/auth/login","method" => "POST"]) !!}

                <div class="form-group">
                    {!! Form::text('email', Input::old('email'), array('class' => 'form-control text-input','placeholder' => 'Email')) !!}
                    @if ($errors->has('email'))
                    <div class="alert alert-danger" style="">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::password('password',array('class' => 'form-control text-input','placeholder'=> 'Password')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Login',array('class'=>'btn btn-success btn-block')) !!}
                </div>
                <a href="{{ url('preset') }}" class="display-block text-center m-t-md text-sm">Forgot Password?</a>
            {!! Form::close() !!}
            <p class="text-center m-t-xs text-sm">2016 &copy; Adventure Earth.</p>
        </div>
    </div>
</div><!-- Row -->
@endsection