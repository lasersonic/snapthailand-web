<?php
/**
* Language file for Banner error/success messages
*
*/

return array(
    'banner_exists'              => 'Banner already exists!',
    'banner_not_found'           => 'Banner [:id] does not exist.',

    'success' => array(
        'create'    => 'Banner was successfully created.',
        'update'    => 'Banner was successfully updated.',
        'delete'    => 'Banner was successfully deleted.',
        'ban'       => 'Banner was successfully banned.',
        'unban'     => 'Banner was successfully unbanned.',
        'suspend'   => 'Banner was successfully suspended.',
        'unsuspend' => 'Banner was successfully unsuspended.',
        'restored'  => 'Banner was successfully restored.'
    ),

    'error' => array(
        'create'    => 'There was an issue creating the banner. Please try again.',
        'update'    => 'There was an issue updating the banner. Please try again.',
        'delete'    => 'There was an issue deleting the banner. Please try again.',
    ),

);
