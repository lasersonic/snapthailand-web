<?php //include 'header.php'; ?>

@extends('layouts.manage_master')

@section('content')

<style>
	.standard-modal{
		max-height: 500px;
		overflow-y: auto;
	}	
	
</style>

<form id="print-submit-form" target="_blank" method="post" action="{{ url('item/printPurchaseOrder') }}" style="display: none;">
	<input type="text" value="" id="purchase-order-id-text" name="purchase_order_id">
	<button type="submit">Submit</button>
</form>

<form id="print-invoice-submit-form" target="_blank" method="post" action="{{ url('manageOrder/printInvoice') }}" style="display: none;">
	<input type="text" value="" id="invoice-id-text" name="invoice_id">
	<input type="text" value="" id="invoice-copy-index-text" name="invoice_copy_index">
	<button type="submit">Submit</button>
</form>

<form id="print-supplier-submit-form" target="_blank" method="post" action="{{ url('manageOrder/printSupplierPurchaseOrder') }}" style="display: none;">
	<input type="text" value="" id="supplier-purchase-order-id-text" name="supplier_purchase_order_id">
	<button type="submit">Submit</button>
</form>


<div class="modal fade" id="purchaseOrderModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">รายการสินค้าที่สั่งซื้อแล้ว</h4>
	      </div>
	      <div class="modal-body standard-modal">
		    <form method="post" action="{{ url('manageOrder/saveShopOrderList') }}">
		      <div class="table-list">
			      <div class="table-responsive">
	
	          	<table id="purchase-order-data-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
	              
	              <thead>
	                  <tr>
	                    	<th style="display: none;"></th>
	                    	<th><span style="color: red;">เลือก</span></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                      <th>จำนวนที่มีอยู่</th>
												<!--
	                      <th><span style="color: red;">จำนวนที่จะจอง</span></th>
	                      -->
	                      <th><span style="color: red;">จำนวนที่จะสั่งเพิ่ม</span></th>
	                  </tr>
	              </thead>
	              <tfoot>
	                  <tr>
	                    	<th style="display: none;"></th>
	                    	<th><span style="color: red;">เลือก</span></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                      <th>จำนวนที่มีอยู่</th>
	                      <!--
	                      <th><span style="color: red;">จำนวนที่จะจอง</span></th>
	                      -->
	                      <th><span style="color: red;">จำนวนที่จะสั่งเพิ่ม</span></th>
	                  </tr>
	              </tfoot>
	              <tbody>
		             	{{--
	                	@foreach($purchase_order_array[$repair_shop_data->rsp_id] as $purchase_order)
	                	
	                		<tr>
	                  		<td style="display: none;">
	                  		<td colspan="7">
	                    		{{ $purchase_order->pod_number }} @ {{ $purchase_order->pod_add_timestamp }}
	                    		<button type="button" class="btn btn-xs btn-info" onclick="printPurchaseOrder(this);"><i class="fa fa-print"></i>&nbsp;&nbsp;พิมพ์เอกสารสั่งซื้อ</button>
	                  		</td>
	                  		<td style="display: none;"></td>
	                  		<td style="display: none;"></td>
	                  		<td style="display: none;"></td>
	                  		<td style="display: none;"></td>
	                  		<td style="display: none;"></td>
	                  		<td style="display: none;"></td>
	                		</tr>
	                		
	                		@foreach($purchase_order->orderRecord as $order_record_data)
	                		<tr>
	                  		<td style="display: none;"></td>
	                			<td>{{ $order_record_data->ord_add_timestamp }}</td>
	                      <td>{{ $order_record_data->item->itm_code }}</td>
	                      <td>{{ $order_record_data->item->itm_barcode }}</td>
	                      <td>{{ $order_record_data->item->itm_name }}</td>
	                      <td>{{ $order_record_data->item->total_count }}</td>
	                      <td>{{ $order_record_data->item->itm_safety_stock }}</td>
	                      <td>{{ $order_record_data->ord_order_number }}</td>
	                		</tr>
	                		@endforeach
											
	                	@endforeach
	                	--}}
	              </tbody>
	              
	              
	            </table>
	
	          </div>
	        </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-info" id="create-invoice-btn" onclick="createInvoiceClicked(this);" data-purchase-order="">สร้าง invoice</button>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	        <button type="submit" class="btn btn-success">บันทึก</button>
	      </div>
	      
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<div class="modal fade" id="supplierPurchaseOrderModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">รายการสินค้าที่สั่ง Supplier ไปแล้ว</h4>
	      </div>
	      <div class="modal-body standard-modal">
		    <form method="post" action="{{ url('manageOrder/saveShopOrderList') }}">
		      <div class="table-list">
			      <div class="table-responsive">
	
	          	<table id="supplier-purchase-order-data-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
	              
	              <thead>
	                  <tr>
	                    	<th style="display: none;"></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>Supplier</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                  </tr>
	              </thead>
	              <tfoot>
	                  <tr>
	                    	<th style="display: none;"></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>Supplier</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                  </tr>
	              </tfoot>
	              <tbody>
		             	<!-- Table Content -->
	              </tbody>
	              
	              
	            </table>
	
	          </div>
	        </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-info" id="create-invoice-btn" onclick="createInvoiceClicked(this);" data-purchase-order="">สร้าง invoice</button>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	        <button type="submit" class="btn btn-success">บันทึก</button>
	      </div>
	      
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->


<div class="modal fade" id="invoiceDataModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">รายการสินค้าใน Invoice</h4>
	      </div>
	      <div class="modal-body standard-modal">
		    <form method="post" action="{{ url('manageOrder/saveShopOrderList') }}">
		      <div class="table-list">
			      <div class="table-responsive">
	
	          	<table id="invoice-display-data-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
	              
	              <thead>
	                  <tr>
	                    	<th style="display: none;"></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัส Invoice</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนสินค้า</th>
	                  </tr>
	              </thead>
	              <tfoot>
	                  <tr>
	                    	<th style="display: none;"></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัส Invoice</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนสินค้า</th>
	                  </tr>
	              </tfoot>
	              <tbody>
		             	<!-- Table Content -->
	              </tbody>
	              
	              
	            </table>
	
	          </div>
	        </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	      </div>
	      
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<div class="modal fade" id="createInvoiceModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">สร้าง Invoice</h4>
	      </div>
	      <div class="modal-body standard-modal">
		    <form method="post" action="{{ url('manageOrder/createNewInvoice') }}" id="create-new-invoice-form">
			    <div class="row" style="padding-bottom: 20px;">
				    <div class="col-md-1"></div>
				    <div class="col-md-10">
					    <input type="hidden" id="create-purchase-order-id-text" name="purchase_order_id">
					    <!--
					    Invoice Number: 
					    <input type="text" id="invoice-number-text" name="invoice_number" class="form-control">
					    -->
				    </div>
				    <div class="col-md-1"></div>
			    </div>
			    
		      <div class="table-list">
			      <div class="table-responsive">
	
	          	<table id="invoice-data-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
	              
	              <thead>
	                  <tr>
	                    	<th style="display: none;"></th>
	                    	<th><span style="color: red;">เลือก</span></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                      <th>จำนวนที่มีอยู่</th>
	                      <th><span style="color: red;">จำนวนที่จะลงใน invoice</span></th>
	                  </tr>
	              </thead>
	              <tfoot>
	                  <tr>
	                    	<th style="display: none;"></th>
	                    	<th><span style="color: red;">เลือก</span></th>
	                      <th>วันที่สั่งสินค้า</th>
	                      <th>รหัสสินค้า</th>
	                      <th>บาร์โค้ด</th>
	                      <th>ชื่อสินค้า</th>
	                      <th>จำนวนที่เริ่มสั่งซื้อ (Safety Stock)</th>
	                      <th>จำนวนที่สั่ง</th>
	                      <th>จำนวนที่มีอยู่</th>
	                      <th><span style="color: red;">จำนวนที่จะลงใน invoice</span></th>
	                  </tr>
	              </tfoot>
	              <tbody>
	              </tbody>
	           
	            </table>
	
	          </div>
	        </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	        <button type="button" class="btn btn-success" onclick="checkAndCreateInvoice();">บันทึก</button>
	      </div>
	      
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<div class="modal fade" id="confirmShopOrderModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">สรุปรายการการสั่งซื้อ</h4>
	      </div>
	      <div class="modal-body standard-modal">
		    <form method="post" action="{{ url('manageOrder/createShopPurchaseOrder') }}">
			    
			    <div class="row" style="padding-top: 5px; padding-bottom: 10px;">
				    <div class="col-md-1"></div>
				    <div class="col-md-10">
					    <span>Supplier</span>
					    <select class="form-control" name="supplier_id">
						    @foreach($supplier_array as $supplier_info)
						    	<option value="{{ $supplier_info->sup_id }}">{{ $supplier_info->sup_company_name }}</option>
						    @endforeach
					    </select>
				    </div>
				    <div class="col-md-1"></div>
			    </div>
			    <!--
			    <div class="row" style="padding-top: 5px; padding-bottom: 20px;">
				    <div class="col-md-1"></div>
				    <div class="col-md-10">
					    <span>รหัสใบสั่งซื้อ</span>
					    <input type="text" class="form-control" name="purchase_order_number">
				    </div>
				    <div class="col-md-1"></div>
			    </div>
			    -->
			    
			    <div class="row">
			      <div class="table-list">
				      <div class="table-responsive">
		
		          	<table id="shop-order-data-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
		              
		              <thead>
		                  <tr>
		                    	<th style="display: none;"></th>
		                    	<th>รหัสสินค้า</th>
		                      <th>บาร์โค้ด</th>
		                      <th>ชื่อสินค้า</th>
		                      <th>จำนวนที่สั่ง</th>
		                      <th>ราคารวม</th>
		                      <th>ลบออก</th>
		                  </tr>
		              </thead>
		              <tfoot>
		                  <tr>
		                    	<th style="display: none;"></th>
		                    	<th>รหัสสินค้า</th>
		                      <th>บาร์โค้ด</th>
		                      <th>ชื่อสินค้า</th>
		                      <th>จำนวนที่สั่ง</th>
		                      <th>ราคารวม</th>
		                      <th>ลบออก</th>
		                  </tr>
		              </tfoot>
		              <tbody>
			             	
		              </tbody>
		              
		              
		            </table>
		
		          </div>
		        </div>
			    </div>
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	        <button type="submit" class="btn btn-success">บันทึก</button>
	      </div>
	      
	      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->


<div class="modal fade" id="invoiceCopySelectModal" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
			<div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">เลือกเอกสาร</h4>
	      </div>
	      
	      <div class="modal-body standard-modal">
			    
			    <div class="row" style="padding-top: 5px; padding-bottom: 15px;">
				    <div class="col-md-2">
					    ใบกำกับภาษี
				    </div>
				    <div class="col-md-4">
					    <div class="pull-right">
						    <button type="button" class="btn btn-info" onclick="printInvoiceWithType(2);">เอกสารสำหรับบัญชี</button>
					    </div>
				    </div>
				    <div class="col-md-6">
					    <div class="pull-left">
						    <button type="button" class="btn btn-success" onclick="printInvoiceWithType(1);">เอกสารสำหรับลูกค้า</button>
					    </div>
				    </div>
			    </div>
			    
			    <div class="row" style="padding-top: 5px; padding-bottom: 15px;">
				    <div class="col-md-2">
					    ใบส่งของ / ใบแจ้งหนี้
				    </div>
				    <div class="col-md-4">
					    <div class="pull-right">
						    <button type="button" class="btn btn-info" onclick="printInvoiceWithType(4);">เอกสารสำหรับบัญชี</button>
					    </div>
				    </div>
				    <div class="col-md-6">
					    <div class="pull-left">
						    <button type="button" class="btn btn-success" onclick="printInvoiceWithType(3);">เอกสารสำหรับลูกค้า</button>
					    </div>
				    </div>
			    </div>
			    
			    <div class="row" style="padding-top: 5px; padding-bottom: 10px;">
				    <div class="col-md-2">
					    ใบเสร็จรับเงิน
				    </div>
				    <div class="col-md-4">
					    <div class="pull-right">
						    <button type="button" class="btn btn-info" onclick="printInvoiceWithType(6);">เอกสารสำหรับบัญชี</button>
					    </div>
				    </div>
				    <div class="col-md-6">
					    <div class="pull-left">
						    <button type="button" class="btn btn-success" onclick="printInvoiceWithType(5);">เอกสารสำหรับลูกค้า</button>
					    </div>
				    </div>
			    </div>
			    
	      </div>
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
	      </div>
	      
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->


<div class="page-title">
    <h3>จัดการรายการสั่งซื้อ</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li><a href="index.html">Consumption Management</a></li>
            <li class="active">จัดการรายการสั่งซื้อ</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
	
		
	
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white panel-table">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title">รายการสั่งซื้อ</h3>
                </div>
                <div class="panel-body">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist">
                            <li role="presentation" class="active">
                            	<a href="#tab-1" role="tab" data-toggle="tab">Purchase Order จาก Workshop</a>
														</li>
														
														<li role="presentation">
                            	<a href="#tab-2" role="tab" data-toggle="tab">Invoice ไปยัง Workshop</a>
														</li>
														
														<li role="presentation">
                            	<a href="#tab-3" role="tab" data-toggle="tab">รายการวัสุดที่จะสั่งไปยัง Supplier</a>
														</li>
														
														<li role="presentation">
                            	<a href="#tab-4" role="tab" data-toggle="tab">Purchase Order ไปยัง Supplier</a>
														</li>
                        </ul>
                        
                        <!-- Tab panes -->
                        <div class="tab-content table-list">
	                        
                            <div role="tabpanel" class="tab-pane active fade in" id="tab-1">
                                <div class="table-responsive">
                                    <table id="table-display-purchase-order" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
                                        <thead>
                                            <tr>
	                                            	<th style="display: none;"></th>
                                                <th>วันที่สั่งสินค้า</th>
											                          <th>Workshop</th>
											                          <th>รหัสการสั่งสินค้า</th>
											                          <th>จำนวนที่สั่ง</th>
											                          <th>จำนวนที่สั่งหรือจองไปแล้ว</th>
											                          <th>จัดการ</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
	                                            	<th style="display: none;"></th>
                                                <th>วันที่สั่งสินค้า</th>
											                          <th>Workshop</th>
											                          <th>รหัสการสั่งสินค้า</th>
											                          <th>จำนวนที่สั่ง</th>
											                          <th>จำนวนที่สั่งหรือจองไปแล้ว</th>
											                          <th>จัดการ</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach($repair_shop_array as $repair_shop_data)
                                        	@foreach($purchase_order_array[$repair_shop_data->rsp_id] as $purchase_order)
												                      <tr>
													                      	<td style="display: none;"></td>
													                      	​<td style="padding: 5px !important;">{{ $purchase_order->pod_add_timestamp }}</td>
													                      	<td>{{ $repair_shop_data->rsp_name }}</td>
												                          <td style="padding: 5px !important;">{{ $purchase_order->pod_number }}</td>
												                          <td style="padding: 5px !important;">{{ count( $purchase_order->orderRecord ) }}</td>
												                          <td style="padding: 5px !important;">
													                          <?php $total_handled = 0; ?>
													                          @foreach($purchase_order->orderRecord as $order_record)
													                          	<?php 
														                          	if( $order_record->shopOrderRecord != null ){
															                          	$total_handled++;
														                          	}	
														                          ?>
													                          @endforeach
													                          {{ $total_handled }}
													                        </td>
												                          <td>
													                          <button type="button" class="btn btn-xs btn-info" data-purchase-order-id="{{ $purchase_order->pod_id }}" onclick="purchaseOrderClicked(this);">ดูข้อมูล</button>
													                          <button type="button" class="btn btn-xs btn-success" data-purchase-order-id="{{ $purchase_order->pod_id }}" onclick="printPurchaseOrder(this);">เอกสาร pdf</button>
													                        </td>
												                      </tr>
											                    @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>  
                                </div>
                        		</div>
                        		
                        		
                        		<div role="tabpanel" class="tab-pane fade in" id="tab-2">
	                        		<div class="row">
		                        		
		                        		<div class="col-md-10"></div>
		                        		<div class="col-md-2">
			                        		<div class="float-right">
				                        		<a href="{{ url('manageOrder/createManualInvoice') }}">
				                        		<button class="btn btn-warning">สร้าง invoice ใหม่</div>
				                        		</a>
			                        		</div>
		                        		</div>
		                        		
		                        		
		                        		<div class="col-md-12">
	                                <div class="table-responsive">
	                                    <table id="table-display-invoice" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
	                                        <thead>
	                                            <tr>
		                                            	<th style="display: none;"></th>
	                                                <th>วันที่สร้าง Invoice</th>
	                                                <th>Workshop</th>
	                                                <th>รหัส Invoice</th>
	                                                <th>จำนวนรายการ</th>
	                                                <th>สถานะ</th>
	                                                <th>จัดการ</th>
	                                            </tr>
	                                        </thead>
	                                        <tfoot>
	                                            <tr>
		                                            	<th style="display: none;"></th>
	                                                <th>วันที่สร้าง Invoice</th>
	                                                <th>Workshop</th>
	                                                <th>รหัส Invoice</th>
	                                                <th>จำนวนรายการ</th>
	                                                <th>สถานะ</th>
	                                                <th>จัดการ</th>
	                                            </tr>
	                                        </tfoot>
	                                        <tbody>
	                                          	@foreach($active_invoice_array as $active_invoice)
	                                          	<tr>
		                                          	<td style="display: none;"></td>
		                                          	<td>{{ $active_invoice->inv_add_timestamp }}</td>
		                                          	<td>{{ $active_invoice->repairShop->rsp_name }}</td>
		                                          	<td>{{ $active_invoice->inv_number }}</td>
		                                          	<td>{{ count( $active_invoice->invoiceItem ) }}</td>
		                                          	<td>{{ $active_invoice->getStatusString() }}</td>
		                                          	<td>
			                                          	<button type="button" class="btn btn-xs btn-info" data-invoice-id="{{ $active_invoice->inv_id }}" onclick="invoiceDataClicked(this);">ดูข้อมูล</button>
			                                          	<button type="button" class="btn btn-xs btn-success" data-invoice-id="{{ $active_invoice->inv_id }}" onclick="printInvoice(this);">เอกสาร pdf</button>
		                                          	</td>
	                                          	</tr>
	                                          	@endforeach
	                                        </tbody>
	                                    </table>  
	                                </div>
		                        		</div>
                        		</div>
                        		
                        		<div role="tabpanel" class="tab-pane fade in" id="tab-3">
                                <div class="table-responsive">
                                    <table id="shop-order-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
                                        <thead>
                                            <tr>
	                                            	<th style="display: none;"></th>
	                                            	<th width="5%">เลือก</th>
	                                            	<th>วันที่สั่ง</th>
                                                <th>รหัสสินค้า</th>
                                                <th>บาร์โค้ด</th>
                                                <th>ชื่อสินค้า</th>
                                                <th>ราคาสั่งซื้อ</th>
                                                <th>จำนวนที่มีใน Stock</th>
                                                <th>จำนวนที่จอง</th>
                                                <th>จำนวนที่จะสั่ง</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
	                                            	<th style="display: none;"></th>
	                                            	<th width="5%">เลือก</th>
	                                            	<th>วันที่สั่ง</th>
                                                <th>รหัสสินค้า</th>
                                                <th>บาร์โค้ด</th>
                                                <th>ชื่อสินค้า</th>
                                                <th>ราคาสั่งซื้อ</th>
                                                <th>จำนวนที่มีใน Stock</th>
                                                <th>จำนวนที่จอง</th>
                                                <th>จำนวนที่จะสั่ง</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        	@foreach($active_shop_order_record_array as $shop_order_record)
																					<tr>
																						<td style="display: none;"></td>
																						<td width="5%">
																							<input type="checkbox" class="form-control" data-shop-order-id="{{ $shop_order_record->sor_id }}" data-item-name="{{ $shop_order_record->item->itm_name }}" data-item-barcode="{{ $shop_order_record->item->itm_barcode }}" data-item-code="{{ $shop_order_record->item->itm_code }}" data-order-number="{{ $shop_order_record->sor_supplier_order_number }}" data-order-price="{{ number_format( ($shop_order_record->item->itm_retail_price * $shop_order_record->sor_supplier_order_number), 2 ) }}" />
																						</td>
																						<td>{{ $shop_order_record->sor_add_timestamp }}</td>
																						<td>{{ $shop_order_record->item->itm_code }}</td>
																						<td>{{ $shop_order_record->item->itm_barcode }}</td>
																						<td>{{ $shop_order_record->item->itm_name }}</td>
																						<!--<td>{{ $shop_order_record->item->itm_suggested_price }}</td>-->
																						<td>{{ $shop_order_record->item->itm_retail_price }}</td>
																						<td>{{ $shop_order_record->item->shop_total_count }}</td>
																						<td>{{ $shop_order_record->sor_reserve_number }}</td>
																						<td>{{ $shop_order_record->sor_supplier_order_number }}</td>
																					</tr>
																					@endforeach
                                        </tbody>
                                    </table>  
                                </div>
                                
                                <hr/>
                                
                                <div>
	                                <div class="pull-right">
		                                <button type="button" class="btn btn-primary" onclick="submitShopOrderClicked();">สร้างใบสั่งซื้อ</button>
	                                </div>
                                </div>
                                
                        		</div>
                        		
                        		
                        		<div role="tabpanel" class="tab-pane fade in" id="tab-4">
	                        		<div class="row">
		                        		
		                        		<div class="col-md-10"></div>
		                        		<div class="col-md-2">
			                        		<div class="float-right">
				                        		<a href="{{ url('manageOrder/orderSupplierItem') }}">
				                        		<button class="btn btn-warning">สร้างใบสั่งซื้อใหม่</div>
				                        		</a>
			                        		</div>
		                        		</div>
		                        		
		                        		
		                        		<div class="col-md-12">
                                	<div class="table-responsive">
                                    <table id="supplier-purchase-order-table" class="display table table table-bordered dataTableBlock" style="width: 100%; cellspacing: 0;">
                                        <thead>
                                            <tr>
	                                            	<th style="display: none;"></th>
                                                <th>ลำดับ</th>
                                                <th>รหัสใบสั่งซื้อ</th>
                                                <th>วันที่</th>
                                                <th>Supplier</th>
                                                <th>จำนวนสินค้าที่สั่ง</th>
                                                <th>จัดการ</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
	                                            	<th style="display: none;"></th>
                                                <th>ลำดับ</th>
                                                <th>รหัสใบสั่งซื้อ</th>
                                                <th>วันที่</th>
                                                <th>Supplier</th>
                                                <th>จำนวนสินค้าที่สั่ง</th>
                                                <th>จัดการ</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        	@foreach($active_supplier_purchase_order_array as $active_supplier_purchase_order)
																					<tr>
																						<td style="display: none;"></td>
																						<td>{{ $active_supplier_purchase_order->spp_running_number }}</td>
																						<td>{{ $active_supplier_purchase_order->spp_number }}</td>
																						<td>{{ $active_supplier_purchase_order->spp_add_timestamp }}</td>
																						<td>{{ $active_supplier_purchase_order->supplier->sup_company_name }}</td>
																						<td>{{ count( $active_supplier_purchase_order->orderRecord ) }}</td>
																						<td>
																							<button type="button" class="btn btn-xs btn-info" onclick="supplierPurchaseOrderClicked(this);" data-supplier-purchase-order-id="{{ $active_supplier_purchase_order->spp_id }}">ดูข้อมูล</button>
																							<button type="button" class="btn btn-xs btn-success" onclick="printSupplierPurchaseOrder(this);" data-supplier-purchase-order-id="{{ $active_supplier_purchase_order->spp_id }}">เอกสาร pdf</button>
																						</td>
																					</tr>
																					@endforeach
                                        </tbody>
                                    </table>  
                                	</div>
		                        		</div>
	                        		</div>
                        		</div>
                        		
                        		
                    		</div>
                    </div>		
                    		
                </div>
            </div>
        </div>
         
</div><!-- Main Wrapper -->
    
    
<script type="text/javascript">
	function purchaseOrderClicked(button){
		//alert($(button).data('purchase-order-id'));
		var purchaseOrderID = $(button).data('purchase-order-id');
		//$('#purchaseOrderModal').modal();
		//alert('purchase order id=' + purchaseOrderID);
		var request = $.post('{{ url('manageOrder/getPurchaseOrderByID') }}', {purchase_order_id: purchaseOrderID});
		request.done( function(response){
			//alert(response);
			
			var itemArray = $.parseJSON(response);
			
			$('#purchase-order-data-table tbody tr').remove();
			
			if(itemArray.length > 0){
				for(index = 0; index < itemArray.length; index++){
					var itemData = itemArray[index];
					
					//$('#purchase-order-data-table tbody').append('<tr><td><input class="form-control" type="checkbox" value="' + itemData['itm_id'] + '" name="item_id[]"></td><td>' + itemData['itm_code'] + '</td><td>' + itemData['itm_barcode'] +'</td><td>' + itemData['itm_name'] +'</td></tr>');
					//alert( JSON.stringify(itemData) );
					if((itemData != undefined) && (itemData['item'] != undefined)){
						$('#purchase-order-data-table tbody').append('<tr><td style="display: none;"></td><td><input type="checkbox" class="form-control" onchange="itemCheckChanged(this);" data-order-id-text="order-id-text-' + itemData['ord_id'] + '" data-reserve-text="reserve-text-' + itemData['ord_id'] + '" data-order-text="order-text-' + itemData['ord_id'] + '"></td><td>' + itemData['ord_add_timestamp'] + '</td><td>' + itemData['item']['itm_code'] + '</td><td>' + itemData['item']['itm_barcode'] + '</td><td>' + itemData['item']['itm_name'] + '</td><td>' + itemData['item']['itm_safety_stock'] + '</td><td>' + itemData['ord_order_number'] + '</td><td>' + itemData['item']['shop_total_count'] + '</td></center></td><!--<td><input type="text" id="reserve-text-' + itemData['ord_id'] + '" name="reserve_number[]" class="form-control text-center" disabled></td>--><td><input type="hidden" value="' + itemData['ord_id'] + '" id="order-id-text-' + itemData['ord_id'] + '" name="order_id[]" class="form-control" disabled><input type="text" id="order-text-' + itemData['ord_id'] + '" name="order_number[]" class="form-control text-center" disabled></td></tr>');
					}
					
				}
			}else{
				$('#purchase-order-data-table tbody').append('<tr><td colspan="8"><center>' + 'No Data to Display' + '</center></td><</tr>');
			}
			
			$('#create-invoice-btn').data('purchase-order', purchaseOrderID);
			
			$('#purchaseOrderModal').modal();
		});
	}	
	
	function invoiceDataClicked(button){
		//alert('Invoice ID = ' + $(button).data('invoice-id'));
		var invoiceID = $(button).data('invoice-id');
		
		var request = $.post('{{ url('manageOrder/getInvoiceByID') }}', {invoice_id: invoiceID});
		request.done( function(response){
			var itemArray = $.parseJSON(response);
			
			//alert(response);
			
			$('#invoice-display-data-table tbody tr').remove();
			
			if(itemArray.length > 0){
				for(index = 0; index < itemArray.length; index++){
					var itemData = itemArray[index];
					
					$('#invoice-display-data-table tbody').append('<tr><td style="display: none;"></td><td>' + itemData['ini_add_timestamp'] + '</td><td>' + itemData['invoice']['inv_number'] + '</td><td>' + itemData['item']['itm_code'] + '</td><td>' + itemData['item']['itm_barcode'] + '</td><td>' + itemData['item']['itm_name'] + '</td><td>' + itemData['ini_item_number'] + '</td></tr>');
					
				}
			}else{
				$('#invoice-display-data-table tbody').append('<tr><td colspan="7"><center>' + 'No Data to Display' + '</center></td><</tr>');
			}
				
			$('#invoiceDataModal').modal();
		});
	}
	
	function supplierPurchaseOrderClicked(button){
		//alert('Supplier PO ID = ' + $(button).data('supplier-purchase-order-id'));
		var supplierPurchaseOrderID = $(button).data('supplier-purchase-order-id');
		
		var request = $.post('{{ url('manageOrder/getSupplierPurchaseOrderByID') }}', {supplier_purchase_order_id: supplierPurchaseOrderID });
		request.done( function(response){
			var itemArray = $.parseJSON(response);
			
			//alert(response);
			
			$('#supplier-purchase-order-data-table tbody tr').remove();
			
			if(itemArray.length > 0){
				for(index = 0; index < itemArray.length; index++){
					var itemData = itemArray[index];
					
					$('#supplier-purchase-order-data-table tbody').append('<tr><td style="display: none;"></td><td>' + itemData['spo_add_timestamp'] + '</td><td>' + itemData['supplier']['sup_company_name'] + '</td><td>' + itemData['item']['itm_code'] + '</td><td>' + itemData['item']['itm_barcode'] + '</td><td>' + itemData['item']['itm_name'] + '</td><td>' + itemData['item']['itm_safety_stock'] + '</td><td>' + itemData['spo_order_number'] + '</td></center></td></tr>');
					
				}
			}else{
				$('#supplier-purchase-order-data-table tbody').append('<tr><td colspan="8"><center>' + 'No Data to Display' + '</center></td><</tr>');
			}
			
			$('#supplierPurchaseOrderModal').modal();
			
		});
	}
	
	
	function createInvoiceClicked(button){
		var purchaseOrderID = $(button).data('purchase-order');
		
		//alert('Try to Create Invoice : ' + purchaseOrderID);
		
		var request = $.post('{{ url('manageOrder/checkItemStock') }}', { purchase_order_id: purchaseOrderID});
		request.done( function(response){
			//alert(response);
			
			var responseArray = $.parseJSON(response);
			//alert(itemArray.length);
			
			if(responseArray['can_create_invoice'] == true){
				$('#purchaseOrderModal').modal('toggle');
				
				$('#create-purchase-order-id-text').val(purchaseOrderID);
				
				var itemArray = responseArray['item_array'];
				
				if(itemArray.length > 0){
					for(index = 0; index < itemArray.length; index++){
						var itemData = itemArray[index];
						
						//$('#purchase-order-data-table tbody').append('<tr><td><input class="form-control" type="checkbox" value="' + itemData['itm_id'] + '" name="item_id[]"></td><td>' + itemData['itm_code'] + '</td><td>' + itemData['itm_barcode'] +'</td><td>' + itemData['itm_name'] +'</td></tr>');
						$('#invoice-data-table tbody').append('<tr><td style="display: none;"></td><td><input type="checkbox" class="form-control" onchange="invoiceItemCheckChanged(this);" data-invoice-order-id-text="invoice-order-id-text-' + itemData['ord_id'] + '" data-invoice-order-text="invoice-order-text-' + itemData['ord_id'] + '"></td><td>' + itemData['ord_add_timestamp'] + '</td><td>' + itemData['item']['itm_code'] + '</td><td>' + itemData['item']['itm_barcode'] + '</td><td>' + itemData['item']['itm_name'] + '</td><td>' + itemData['item']['itm_safety_stock'] + '</td><td>' + itemData['ord_order_number'] + '</td><td>' + itemData['item']['shop_total_count'] + '</td></center></td><td><input type="hidden" value="' + itemData['ord_id'] + '" id="invoice-order-id-text-' + itemData['ord_id'] + '" name="order_id[]" class="form-control" disabled><input type="text" id="invoice-order-text-' + itemData['ord_id'] + '" name="order_number[]" class="form-control text-center" disabled></td></tr>');
					}
				}else{
					$('#invoice-data-table tbody').append('<tr><td colspan="8"><center>' + 'No Data to Display' + '</center></td><</tr>');
				}
				
				
				$('#createInvoiceModal').modal();
			}else{
				alert(responseArray['error']);
			}		
			
		});
		
	}
	
	function checkAndCreateInvoice(){
		//alert('Check And Create Invoice');
		var invoiceNumber = $('#invoice-number-text').val();
		var purchaseOrderID = $('#create-purchase-order-id-text').val();
		
		//alert('invoice = ' + invoiceNumber + ' / purchase order id = ' + purchaseOrderID);
		
		var checkCount = 0;
		$('#invoice-data-table input[type="checkbox"]').each(function() {
		    //alert($(this).val());
		    if($(this).prop('checked') == true){
			    checkCount = checkCount + 1;
		    }
		    //else{
			  //  alert('Is NOT Checked');
		    //}
		});
		
		//if((invoiceNumber.length > 0) && (checkCount > 0)){
		if(checkCount > 0){
			$('#create-new-invoice-form').submit();
		}else{
			alert('Please provide invoice number and select at least 1 item to be in the invoice');
		}
		
	}
	
	
</script>

<script type="text/javascript">
	function itemCheckChanged(checkBox){
		//alert('Item Check on Change');
		//alert($(checkBox).prop('checked'));
		
		if($(checkBox).prop('checked') == true){
			//alert('#' + $(checkBox).data('reserve-text'));
			$('#' + $(checkBox).data('order-id-text')).removeAttr('disabled');
			$('#' + $(checkBox).data('reserve-text')).removeAttr('disabled');
			$('#' + $(checkBox).data('order-text')).removeAttr('disabled');
		}else{
			//$("input").attr('disabled','disabled');
			$('#' + $(checkBox).data('order-id-text')).attr('disabled','disabled');
			$('#' + $(checkBox).data('reserve-text')).attr('disabled','disabled');
			$('#' + $(checkBox).data('order-text')).attr('disabled','disabled');
		}
		
	}
	
	function invoiceItemCheckChanged(checkBox){
		//alert('Invoice Item Check on Change');
		//alert($(checkBox).prop('checked'));
		
		if($(checkBox).prop('checked') == true){
			//alert('#' + $(checkBox).data('reserve-text'));
			$('#' + $(checkBox).data('invoice-order-id-text')).removeAttr('disabled');
			$('#' + $(checkBox).data('invoice-order-text')).removeAttr('disabled');
		}else{
			//$("input").attr('disabled','disabled');
			$('#' + $(checkBox).data('invoice-order-id-text')).attr('disabled','disabled');
			$('#' + $(checkBox).data('invoice-order-text')).attr('disabled','disabled');
		}
		
	}
</script>

<script type="text/javascript">
	function printPurchaseOrder(button){
		//alert('Print Purchase Order');
		
		//$('#purchaseOrderSelectModal').modal();
		
		$('#purchase-order-id-text').val( $(button).data('purchase-order-id') );
		$('#print-submit-form').submit();		
	}
	/*
	function printPurchaseOrderForClient(button){
		//alert('Print Purchase Order');
		
		$('#purchaseOrderSelectModal').modal();
		
		//$('#purchase-order-id-text').val( $(button).data('purchase-order-id') );
		$('#purchase-order-client-copy-text').val( 1 );
		$('#print-submit-form').submit();		
	}	
	
	function printPurchaseOrderForAccounting(button){
		//alert('Print Purchase Order');
		
		$('#purchaseOrderSelectModal').modal();
		
		//$('#purchase-order-id-text').val( $(button).data('purchase-order-id') );
		$('#purchase-order-client-copy-text').val( 0 );
		$('#print-submit-form').submit();		
	}
	*/
	
	function printInvoice(button){
		//alert('Print Purchase Order');
		
		$('#invoiceCopySelectModal').modal();
		$('#invoice-id-text').val( $(button).data('invoice-id') );
		//$('#print-invoice-submit-form').submit();		
	}	
	
	function printInvoiceWithType(type){
		//alert('Print Purchase Order');
		
		$('#invoiceCopySelectModal').modal();
		//$('#invoice-id-text').val( $(button).data('invoice-id') );
		$('#invoice-copy-index-text').val( type );
		$('#print-invoice-submit-form').submit();		
	}	
	
	function printSupplierPurchaseOrder(button){
		//alert('Print Purchase Order');
		$('#supplier-purchase-order-id-text').val( $(button).data('supplier-purchase-order-id') );
		$('#print-supplier-submit-form').submit();		
	}	
	
</script>

<script type="text/javascript">
	function submitShopOrderClicked(){
		//alert('Submit Shop Order Clicked');
		
		$('#shop-order-data-table tbody tr').remove();
		
		$('#shop-order-table input:checked').each(function() {
		    //selected.push($(this).attr('name'));
		    //alert($(this).data('item-name'));
		    
		    $('#shop-order-data-table tbody').append('<tr><td style="display: none;"></td><td>' + $(this).data('item-code') + '</td><td>' + $(this).data('item-barcode') + '</td><td>' + $(this).data('item-name') + '</td><td><input type="text" class="form-control text-center" name="order_number[]" value="' + $(this).data('order-number') + '"></td><td><input type="text" class="form-control text-center" name="order_price[]" value="' + $(this).data('order-price') + '"></td><td><input type="hidden" value="' + $(this).data('shop-order-id') + '" name="shop_order_id[]" class="form-control"><button type="button" class="btn btn-warning btn-xs btn-block" onclick="removeOrderData(this);" data-item-name="' + $(this).data('item-name') + '">ลบออก</button></td></tr>');
		    
		});
		
		$('#confirmShopOrderModal').modal();
	}	
</script>

<script type="text/javascript">
	function removeOrderData(button){
		//$(button).hide();
		if( confirm('Do you want to remove ' + $(button).data('item-name') + ' from this purchase order?') == true ){
			$(button).parent().parent().remove();
		}
	}
</script>

<script>
    $('.horizontal-bar .page-sidebar-inner > ul > li:nth-child(1)').removeClass('active');
    $('.horizontal-bar .page-sidebar-inner > ul > li:nth-child(4)').addClass('active');
    //$('.horizontal-bar .page-sidebar-inner > ul > li.active li:nth-child(4)').addClass('active');
</script>

@endsection

<?php //include 'footer.php'; ?>