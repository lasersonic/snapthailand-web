<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class SentinelRoles {

    public function handle($request, Closure $next, ...$roles)
    {
        $not_permission = true;

        foreach($roles as $role) {
            if(Sentinel::inRole($role)) {
                $not_permission = false;
                break;
            }
        }

        if($not_permission) {
            if ($request->ajax()) {
                return response('Unauthorized.', 403);
            } else {
                return response()->view('errors.other', ['message' => trans('error.message_403')], 403);
            }
        }

        return $next($request);
    }
}