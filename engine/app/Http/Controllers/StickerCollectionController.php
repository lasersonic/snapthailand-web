<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Database\QueryException as QueryException;

use Sentinel;
use Lang;
use Validator;
use Storage;
use File;

use App\Models\StickerCollection;
use App\Models\Sticker;
use App\Models\PluploadHandler;

class StickerCollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
//        $this->middleware('sentinel.roles:admin');
    }

    public function getIndex()
    {
        $models = StickerCollection::recently()->get();
        $trashCount = (($count = StickerCollection::onlyTrashed()->count()) == 0) ? '' : $count;

        return view('sticker-collections.index', compact('models', 'trashCount','draftCount'));
    }

    public function getTrash()
    {
        $models = StickerCollection::onlyTrashed()->get();
        $trashCount = (($count = StickerCollection::onlyTrashed()->count()) == 0) ? '' : $count;

        return view('sticker-collections.trash', compact('models', 'trashCount'));
    }

    public function anyCreate($id=0)
    {
        try {
            $model = null;

            if ($id!=0) {
                $model = StickerCollection::findOrFail($id);
            } else {
                $model = new StickerCollection();
                $model->status = 0;
                $model->price = 0;
                $model->name = "";
                $model->description = "";
                $model->android_inapp_id = "";
                $model->ios_inapp_id = "";

                if ($model->save()) {
                    return redirect()->to('sticker-collections/create/'.$model->id);
                }
            }

            if (Request::isMethod('post')) {
                $model->name = Request::get('name');
                $model->description = Request::get('description');
                $model->price = Request::get('price');
                $model->status = Request::get('status');
                $model->android_inapp_id = Request::get('android_inapp_id');
                $model->ios_inapp_id = Request::get('ios_inapp_id');

                $validator = $this->_validate(Request::all(), 'update');

                if($model->status) {
                    $model->locked = true;
                }

                if ($validator->passes()) {
                    $model->save();

                    return redirect()->to('sticker-collections')->with('success', Lang::get('sticker-collections/message.success.create'));
                } else {
                    return redirect()->back()->with(compact($model))->withInput()->withErrors($validator->errors());
                }

            }

            return view('sticker-collections.create', compact('model'));
        } catch (ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        } catch(QueryException $e) {
            return view('errors.500', ['title' => 'Sticker Collections', 'message' => $e->getMessage(), 'url' => url('/sticker-collections')]);
        } catch(Exception $e) {
            return view('errors.500', ['title' => 'Sticker Collections', 'message' => $e->getMessage(), 'url' => url('/sticker-collections')]);
        }
    }

    public function anyUpdate($id)
    {
        try {
            $model = StickerCollection::findOrFail($id);

            if (Request::isMethod('post')) {
                $model->name = Request::get('name');
                $model->description = Request::get('description');
                $model->price = Request::get('price');

                if(!(bool)$model->locked) {
                    $model->status = Request::get('status');
                    $model->android_inapp_id = Request::get('android_inapp_id');
                    $model->ios_inapp_id = Request::get('ios_inapp_id');
                }

                if($model->status) {
                    $model->locked = true;
                }

                $validator = $this->_validate(Request::all(), 'update');

                if ($validator->passes()) {
                    $model->save();
                    $model->generateStickerZipFile();
                    return redirect()->to('sticker-collections')->with('success', Lang::get('sticker-collections/message.success.update'));
                } else {
                    return redirect()->back()->with(compact($model))->withInput()->withErrors($validator->errors());
                }

            }

            return view('sticker-collections.update', compact('model'));

        } catch (ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        } catch(QueryException $e) {
            return view('errors.500', ['title' => 'Sticker Collections', 'message' => $e->getMessage(), 'url' => url('/sticker-collections')]);
        } catch(Exception $e) {
            return view('errors.500', ['title' => 'Sticker Collections', 'message' => $e->getMessage(), 'url' => url('/sticker-collections')]);
        }
    }

    public function getDelete($id)
    {
        $model = StickerCollection::find($id);

        if (!$model) {
            return redirect()->to('sticker-collections')->with('error', Lang::get('sticker-collections/message.not_found', compact('id')));
        }

        StickerCollection::destroy($id);
        return redirect()->to('sticker-collections')->with('success', Lang::get('sticker-collections/message.success.delete'));
    }

    public function getStickerDelete($id) {
        /** @var Sticker $model */
        $model = Sticker::find($id);

        if (!$model) {
            return redirect()->to('sticker-collections/update/'.$model->sticker_collection_id)->with('error', Lang::get('sticker-collections/message.sticker_not_found', compact('id')));
        }

        $model->delete();
        return redirect()->to('sticker-collections/update/'.$model->sticker_collection_id)->with('success', Lang::get('sticker-collections/message.success.sticker-delete'));
    }

    public function getRestore($id = null)
    {
        $model = StickerCollection::onlyTrashed()->find($id);
        if (!$model) {
            return redirect()->to('sticker-collections/trash')->with('error', Lang::get('sticker-collections/message.not_found', compact('id')));
        }

        $model->restore();
        return redirect()->to('sticker-collections/trash')->with('success', Lang::get('sticker-collections/message.success.restored'));
    }

    public function getTrashDelete($id)
    {
        $model = StickerCollection::onlyTrashed()->find($id);

        if (!$model) {
            return redirect()->to('sticker-collections')->with('error', Lang::get('sticker-collections/message.not_found', compact('id')));
        }

        $model->forceDelete();
        return redirect()->to('sticker-collections')->with('success', Lang::get('sticker-collections/message.success.delete'));
    }

    public function getStickerModalDelete($id = null) {
        $model_name = 'sticker-collections';
        $model_action = 'sticker-delete';
        $confirm_route = $error = null;

        $model = Sticker::find($id);

        if (!$model) {
            $error = Lang::get($model_name . '/message.not_found', compact('id'));
            return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url($model_name . '/sticker-delete', ['id' => $model->id]);
        return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    public function getModalDelete($id = null)
    {
        $model_name = 'sticker-collections';
        $model_action = 'delete';
        $confirm_route = $error = null;

        $model = StickerCollection::find($id);

        if (!$model) {
            $error = Lang::get($model_name . '/message.not_found', compact('id'));
            return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url($model_name . '/delete', ['id' => $model->id]);
        return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    public function getModalTrashDelete($id = null)
    {
        $model_name = 'sticker-collections';
        $model_action = 'trash-delete';
        $confirm_route = $error = null;

        $model = StickerCollection::onlyTrashed()->find($id);

        if (!$model) {
            $error = Lang::get($model_name . '/message.not_found', compact('id'));
            return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url($model_name . '/trash-delete', ['id' => $model->id]);
        return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    public function getModalRestore($id = null)
    {
        $model_name = 'sticker-collections';
        $model_action = 'restore';
        $confirm_route = $error = null;

        $model = StickerCollection::onlyTrashed()->find($id);

        if (!$model) {
            $error = Lang::get($model_name . '/message.not_found', compact('id'));
            return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
        }

        $confirm_route = url($model_name . '/restore', ['id' => $model->id]);
        return view('layouts.modal', compact('error', 'model_name', 'model_action', 'confirm_route'));
    }

    // Upload sticker handler
    public function postUploadSticker($id)
    {
        PluploadHandler::no_cache_headers();
        PluploadHandler::cors_headers();

        try {
            $model = StickerCollection::findOrFail($id);
            $response = null;

            $file_name = uniqid('sticker_') . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

            $handle_config = [
                'target_dir' => $model->getUploadDir(),
                'allow_extensions' => 'jpg,jpeg,png',
                'file_name' => $file_name
            ];

            if (PluploadHandler::handle($handle_config)) {
                $response = ['OK'=>1];

                $sticker = new Sticker();
                $sticker->sticker_collection_id = $model->id;
                $sticker->image = $file_name;
                $sticker->sort_order = Sticker::getLastOrder($model->id);
                $sticker->save();
            } else {
                $response = [
                    'OK' => 0,
                    'error' => [
                        'code' => PluploadHandler::get_error_code(),
                        'message' => PluploadHandler::get_error_message()
                    ]
                ];
            }

            return response()->json($response);
        } catch (ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        }
    }

    public function getUpdateStickerZipFile($id)
    {
        try {
            $model = StickerCollection::findOrFail($id);
            $model->generateStickerZipFile();
            $response = [
                'OK' => 1,
            ];

            return response()->json($response);
        } catch (ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        }
    }
    
    public function postStickerOrder($id) {
        try {
            $model = StickerCollection::findOrFail($id);
            $response = null;

            $stickerOrders = json_decode($_POST['order']);

            foreach($stickerOrders as $i => $stickerOrder) {
                $sticker = Sticker::where('sticker_collection_id',$model->id)
                    ->where('id',$stickerOrder->id)
                    ->first();

                if($sticker) {
                    $sticker->sort_order = $i+1;
                    $sticker->save();
                }
            }

            return response()->json(['OK'=>1]);
        } catch(ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        }
    }

    public function postUpdateAvatar($id) {
        try {
            $model = StickerCollection::findOrFail($id);
            $model->avatar_sticker_id = $_POST['sticker_id'];
            $model->save();

            return response()->json(['OK'=>1,'image'=>$model->getAvatarStickerImageSrc()]);
        } catch(ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        }
    }

    public function postUpdateThumbnails($id) {
        try {
            $model = StickerCollection::findOrFail($id);
            $stickers_id = explode(",",$_POST['stickers_id']);

            $images = [];
            foreach($model->stickers as $sticker) {
                if(in_array($sticker->id, $stickers_id)) {
                    $sticker->use_as_thumb = 1;
                    $images[] = $sticker->getImageThumbnailSrc();
                } else {
                    $sticker->use_as_thumb = 0;
                }

                $sticker->save();
            }

            $currentAttributeCount = count($images);

            if($currentAttributeCount < 2) {
                for($i=$currentAttributeCount;$i<2;$i++) {
                    $images[] = url('/assets/images/sticker-holder.png');
                }
            }

            return response()->json(['OK'=>1,'images'=>$images]);
        } catch(ModelNotFoundException $e) {
            return view('errors.404', ['title' => 'Sticker Collections', 'url' => url('/sticker-collections')]);
        }
    }

    public function getClearStickers() {
        $models = StickerCollection::withTrashed()->get();

        $collections = [];
        foreach($models as $model) {
            $collections[] = $model->sticker_collection_id;
        }

        print_r($collections);
        $files = File::directories(public_path() . '/../../uploads/sticker_collections');

        foreach($files as $file) {
            $dir = substr(strrchr($file, "/"), 1);

            if(in_array($dir,$collections)) {
                echo $dir . "<br />";
                //File::deleteDirectory($dir, true);
            }
        }


//        $success = File::deleteDirectory($directory, true);
    }

    // Validate
    private function _validate($input, $scenario, $id = null)
    {
        $rules = array();

        switch ($scenario) {
            case 'create':
            case 'update':
                $rules = array(
                    'name' => 'required|min:3',
                    'description' => 'min:3',
                    'status' => 'in:1,0',
                );
                break;
        }

        return Validator::make($input, $rules);
    }
}