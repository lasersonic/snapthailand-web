<?php
namespace App\Http\Controllers\APIV1;

use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

use App\Http\Controllers\Controller;
use App\Models\StickerCollection;
use App\Models\Download;
use Illuminate\Http\Request;

class StickerCollectionController extends Controller
{
    public function index()
    {
        $models = StickerCollection::with('stickers')->published()->recently()->get();
        return $this->sendResponse(200, 'OK', $models->toArray());
    }

    public function view($id)
    {
        try {
            $stickerCollection = StickerCollection::published()
                ->where('id', '=' ,$id)
                ->orWhere('ios_inapp_id','=',$id)
                ->orWhere('android_inapp_id','=',$id)
                ->firstOrFail();

            return $this->sendResponse(200, 'OK', $stickerCollection->toArray());
        } catch (ModelNotFoundException $e) {
            return $this->sendResponse(404);
        }
    }

    public function purchased(Request $request)
    {
        $data = $request->input('data');

        // Check request
        if(!$data) return $this->sendResponse(400,'Invalid Request');
        else {
            $json = json_decode($data, true);

            if(is_array($json)) {
                if(!array_key_exists('stickerIds',$json)) {
                    return $this->sendResponse(400,'Invalid Request: stickerIds');
                }

                $reqIds = $json["stickerIds"];

                $stickerCollections = StickerCollection::published()
                    ->whereIn('id',$reqIds)
                    ->orWhereIn('ios_inapp_id',$reqIds)
                    ->orWhereIn('android_inapp_id',$reqIds)
                    ->get();

                return $this->sendResponse(200,'OK',$stickerCollections->toArray());
            } else {
                return $this->sendResponse(400,'Invalid Request: no data');
            }
        }
    }

    public function download(Request $request)
    {
        $data = $request->input('data');

        // Check request
        if(!$data) return $this->sendResponse(400,'Invalid Request');
        else {
            $json = json_decode($data,true);

            if(is_array($json)) {
                if(!array_key_exists('sticker_collection_id',$json)) {
                    return $this->sendResponse(400,'Invalid Request: sticker_collection_id');
                }

                if(!array_key_exists('platform',$json)) {
                    $this->sendResponse(400,'Invalid Request: platform');
                }

                if(!array_key_exists('device_id',$json)) {
                    $this->sendResponse(400,'Invalid Request: device_id');
                }
            } else {
                return $this->sendResponse(400,'Invalid Request: no data');
            }
        }

        try {
            $stickerCollection  = StickerCollection::published()->where('id','=',$json['sticker_collection_id'])
                ->orWhere('ios_inapp_id','=',$json['sticker_collection_id'])
                ->orWhere('android_inapp_id','=',$json['sticker_collection_id'])
                ->firstOrFail();

            $download = new Download();
            $download->sticker_collection_id = $stickerCollection->id;
            $download->platform = $json['platform'];
            $download->device_id = $json['device_id'];
            if($download->save()) {
                $stickerCollection->download_count++;
                $stickerCollection->save();
                return redirect()->away($stickerCollection->download_link);
            }

            return $this->sendResponse(200,'OK');
        } catch (ModelNotFoundException $e) {
            return $this->sendResponse(404);
        }
    }

}