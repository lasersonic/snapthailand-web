<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected  function sendResponse($status_code=200,$message='',$data=null)
    {
        if($message == '') {
            $message = $this->getStatusCodeMessage($status_code);
        }

        if(is_array($message)) {
            $description = '';

            foreach($message[1] as $each) {
                foreach($each as $text) {
                    $description .= $text."\n";
                }
            }

            $response['meta'] = array(
                'status_code'	=> $status_code,
                'message'		=> ($message[0]=='')? $this->getStatusCodeMessage($status_code) : $message[0],
                'description'	=> $description
            );
        } else {
            $response['meta'] = array(
                'status_code'	=> $status_code,
                'message'		=> $message
            );
        }

        $response['data'] = array();

        if($data != null)
        {
            $response['data'] = $data;
        }

        return response()->json($response);
    }

    protected function getStatusCodeMessage($status)
    {
        $codes = Array(
            200 => 'OK',
            204	=> 'No Content',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    protected function getSingleton($class)
    {
        return app()->make($class);
    }
}
