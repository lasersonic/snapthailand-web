<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

use Sentinel;
use Lang;
use Redirect;
use Validator;

use App\Models\Order;

class OrderController extends Controller {
    public function __construct()
    {
        $this->middleware('sentinel.auth');
//        $this->middleware('sentinel.roles:admin');
    }

    public function getIndex()
    {
        $models = Order::recently()->get();
        return View('orders.index', compact('models'));
    }
} 