<?php
namespace App\Http\Controllers;

use View;
use Sentinel;
use Input;
use Reminder;
use Event;
use App\Events\ReminderEvent;
use Redirect;
use Session;

class ReminderController extends Controller
{


    public function create() {
        return View::make('reminder.create');
    }

    public function store() {
        $user = Sentinel::findByCredentials(['login'=>Input::get('email')]);

        if ($user) {
            ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));
            Event::fire(new ReminderEvent($user, $reminder));
        } else {
            return Redirect::back()->withInput()->withErrors(['email'=>'Sorry! This email doesn\'t exist']);
        }

        return redirect()->to('/auth/login')->with('success', 'Password reset link email has been sent to your email address');
    }

    public function edit($id, $code) {
        $user = Sentinel::findById($id);

        if (Reminder::exists($user, $code)) {
            return View::make('reminder.edit', ['id' => $id, 'code' => $code]);
        }
        else {
            //incorrect info was passed
            return redirect()->to('/auth/login')->with('warning', 'Your password reset request has expired.');
        }
    }

    public function update($id, $code) {
        $password = Input::get('password');
        $passwordConf = Input::get('password_confirmation');

        $user = Sentinel::findById($id);
        $reminder = Reminder::exists($user, $code);

        //incorrect info was passed.
        if ($reminder == false) {
            return redirect()->to('/auth/login')->with('warning', 'Your password reset request has expired.');
        }

        if ($password != $passwordConf) {
            Session::flash('error', 'Passwords must match.');
            return View::make('reminder.edit', ['id' => $id, 'code' => $code]);
        }

        Reminder::complete($user, $code, $password);
        return redirect()->to('/auth/login')->with('success', '<b>Password Reset Complete.</b><br />Your new password is now active to login.');
    }
}