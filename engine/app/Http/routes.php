<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'HomeController@index');

Route::controllers([
    'home' => 'HomeController',
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'banners' => 'BannerController',
    'sticker-collections' => 'StickerCollectionController',
    'orders' => 'OrderController',
    'users' => 'UserController',
]);

Route::get('passwordreset/{id}/{token}', ['as' => 'reminders.edit', 'uses' => 'ReminderController@edit']);
Route::post('passwordreset/{id}/{token}', ['as' => 'reminders.update', 'uses' => 'ReminderController@update']);
Route::get('passwordreset',['as'=>'preset','uses'=> 'ReminderController@create']);
Route::post('passwordreset', 'ReminderController@store');

Route::get('/api-document', function () {
    return View('home.api_document');
});

// Route for API
Route::group(['prefix'=>'api/v1'],function() {

    Route::get('sticker-collections', 'APIV1\StickerCollectionController@index');
    Route::get('sticker-collections/{id}', 'APIV1\StickerCollectionController@view');
    Route::post('sticker-collections/purchased', 'APIV1\StickerCollectionController@purchased');
    Route::post('sticker-collections/download', 'APIV1\StickerCollectionController@download');

    Route::post('orders/purchase', 'APIV1\OrderController@purchase');
});

Route::post('/login', function () {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        return 'OK with ' . $_POST['username'] . ':' . $_POST['password'];
    } else {
        return 'Fail';
    }
});