<?php
namespace App\Events;


use Illuminate\Queue\SerializesModels;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cartalyst\Sentinel\Reminders\EloquentReminder;

class ReminderEvent extends Event
{
    use SerializesModels;
    public $user;
    public $reminder;

    public function __construct(EloquentUser $user, EloquentReminder $reminder)
    {
        $this->user = $user;
        $this->reminder = $reminder;
    }
}