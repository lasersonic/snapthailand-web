<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $table = 'banners';
    protected $fillable = [];
    protected $hidden = [];

    protected $dates = ['deleted_at'];

    public function scopePriority($query)
    {
        return $query->orderBy('sort_order');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
