<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{

    protected $table = 'orders';
    protected $fillable = [];
    protected $hidden = [];

    public function scopeRecently($query)
    {
        return $query->orderBy('created_at','desc');
    }

    /*
     * Relational
     */
    public function stickerCollection() {
        return $this->belongsTo('App\Models\StickerCollection','sticker_collection_id');
    }

    public function getHumanDate($field) {
        $date = $this->{$field};
        $humanDate = null;

        if($date->isToday()) {
            $humanDate = 'Today ' . $date->format('H:i');
        } else if($date->isYesterday()) {
            $humanDate = 'Yesterday ' . $date->format('H:i');
        } else {
            $humanDate = $date->format('M j');
        }

        return $humanDate;
    }
}
